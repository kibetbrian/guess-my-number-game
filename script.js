'use strict';

/*
console.log(document.querySelector('.message').textContent);

document.querySelector('.message').textContent = 'Correct!';

document.querySelector('.number').textContent = 24;
document.querySelector('.message').textContent = 'Wow you did it!';
document.querySelector('.highscore').textContent = 145;


document.querySelector('.guess').value = 65;
console.log(document.querySelector('.guess').value);
*/
let secretNumber = Math.trunc(Math.random() * 20) + 1;
let score = 20;
let highscore = 0;

const displayMessage = function(message) {
    document.querySelector('.message').textContent = message;
}

const displayNumber = function(numb) {
    document.querySelector('.number').textContent = numb;
}

document.querySelector('.check').addEventListener('click', function() {
    const guess = Number(document.querySelector('.guess').value);
    // console.log(guess, typeof guess)

    //When There is No Guess
    if (!guess) {
        // document.querySelector('.message').textContent = '⛔ No Number!';
        displayMessage('⛔ No Number!');
    }
    // When  Guess Is Correct
    else if (guess === secretNumber) {
        document.querySelector('body').style.backgroundColor = '#60b347';
        document.querySelector('.number').style.width = '30rem';
        // document.querySelector('.message').textContent = '🎉 Correct Number!';
        displayMessage('🎉 Correct Number!');
        // document.querySelector('.number').textContent = secretNumber;
        displayNumber(secretNumber);
        if (score > highscore) {
            highscore = score;
            document.querySelector('.highscore').textContent = highscore;
        }
    }

    // When  Guess Is Not Correct
    else if (guess !== secretNumber) {
        if (score > 1) {
            // document.querySelector('.message').textContent = guess > secretNumber ? '🔼 Too High!' : '🔽 Too Low!';
            displayMessage(guess > secretNumber ? '🔼 Too High!' : '🔽 Too Low!');

            // score = score - 1;
            score--;
            document.querySelector('.score').textContent = score;
        } else {
            // document.querySelector('.message').textContent = '💥 You lost The Game!';
            displayMessage('💥 You lost The Game!');
            document.querySelector('.score').textContent = 0;
        }
    }

    // // When  Guess Is Too High
    // else if (guess > secretNumber) {
    //     if (score > 1) {
    //         document.querySelector('.message').textContent = '🔼 Too High!';
    //         // score = score - 1;
    //         score--;
    //         document.querySelector('.score').textContent = score;
    //     } else {
    //         document.querySelector('.message').textContent = '💥 You lost The Game!';
    //         document.querySelector('.score').textContent = 0;
    //     }
    // }
    // // When  Guess Is Too Loow
    // else if (guess < secretNumber) {
    //     if (score > 0) {
    //         document.querySelector('.message').textContent = '🔽 Too Low!';
    //         // score = score - 1;
    //         score--;
    //         document.querySelector('.score').textContent = score;
    //     } else {
    //         document.querySelector('.message').textContent = '💥 You lost The Game!';
    //         document.querySelector('.score').textContent = 0;
    //     }

    // }
});

document.querySelector('.again').addEventListener('click', function() {
    score = 20;
    secretNumber = Math.trunc(Math.random() * 20) + 1;
    // document.querySelector('.message').textContent = 'Start guessing...';
    displayMessage('Start guessing...');
    document.querySelector('.score').textContent = score;
    document.querySelector('body').style.backgroundColor = ' #222';
    // document.querySelector('.number').textContent = '?';
    displayNumber('?');
    document.querySelector('.guess').value = '';

});